from atlassian import Confluence
from pathlib import Path
from botocore.exceptions import ClientError
import boto3
import os, re, shutil
import pandas as pd, pdfkit
import sys 
import glob
import logging

# Set global variables
bucket_name = 'esp-sre-runbooks'
endpoint = 'https://confluence.eng.vmware.com'
user = os.environ.get('CONFLUENCE_USERNAME')
pwd = os.environ.get('CONFLUENCE_PASSWORD')
page_identifier = os.environ.get('CONFLUENCE_PAGE_ID')
confluence = Confluence(
    url=endpoint,
    username=user,
    password=pwd
)

# Setup logging configuration 
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def upload_runbook_to_s3(file_name):
    s3_client = boto3.client('s3')
    try:
       object_key = "%s" % os.path.basename(file_name)
       logging.info("Uploading the backup file: " + file_name + "...")
       s3_client.upload_file(file_name, bucket_name, object_key)
    except ClientError as e:
        logging.error(e)
    logging.info("Successfully uploaded the file: " + file_name + " to the following s3 bucket: " + bucket_name)
    os.remove(file_name)

def main(): 
    index = []
    data = confluence.get_child_pages(page_id=page_identifier)
    for j in data:
        runbooks = confluence.get_subtree_of_content_ids(page_id=j["id"])
        for i in runbooks:
            try:
                ancestors = confluence.get_page_ancestors(page_id=i)
                relative = "/".join([re.sub("[~\"#%&*:<>?/{|}.]", "-", i['title']) for i in ancestors])
                _dir = f"/opt/confluence/{relative}"
                page = confluence.get_page_by_id(page_id=i)
                os.makedirs(name=_dir, exist_ok=True)
                pdf = confluence.export_page(page_id=i)
                title = re.sub("[~\"#%&*:<>?/{|}.]", "-", page['title'])
                file_name = "{dir}/{title}".format(dir=_dir, title=title)
                index.append([
                    f'<b>Run Book Name</b>: {title}<br> '
                    f'<b>Run Book Tiny URL</b>: {endpoint}{page["_links"]["tinyui"]}<br>'
                    f'<b>Run Book WebUI URL</b>: {endpoint}{page["_links"]["webui"]}<br>'
                    f'<b>Run Book Path</b>: {relative}<br>'
                ])
                with open(f"{file_name}.pdf", "wb") as f:
                    f.write(pdf)
            except Exception as e:
                print(e)

    df = pd.DataFrame(index, columns=["RunBook Path, Tiny URL & WebUI URL"])
    df.to_html("/opt/confluence/index.html", escape=False, header=True,
            bold_rows=True, show_dimensions=True, justify="center")

    for root, directories, filenames in os.walk('/opt/confluence/'):
        for filename in filenames:
            abs_file_name = os.path.join(root, filename)
            my_path = Path(abs_file_name.replace(".pdf", ""))
            if my_path.is_dir():
                dir_name = abs_file_name.replace(".pdf", "")
                shutil.move(abs_file_name, dir_name + "/" + filename)

    os.chdir('/opt/confluence/')
    runbooks = glob.glob("*.pdf")

    for root, directories, filenames in os.walk('/opt/confluence/'):
        for filename in filenames:
            abs_file_name = os.path.join(root, filename)
            my_path = Path(abs_file_name.replace(".pdf", ""))
            if not my_path.is_dir():
                logging.info('Uploading runbook: ' + abs_file_name)
                upload_runbook_to_s3(abs_file_name)

if __name__ == "__main__":
    main()
