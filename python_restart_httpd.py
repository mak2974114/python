import requests
import subprocess
import os, paramiko
import time, json, logging, schedule
from datetime import datetime

logging.basicConfig(filename='api_request_log.txt', level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')

#credential save in root .bash_profile
username = os.environ.get('ssh_user_name')
password = os.environ.get('ssh_password')

#prod and dr api hosts
servers = [
    {"hostname": "10.170.156.51", "username": username, "password": password},
    {"hostname": "10.170.156.96", "username": username, "password": password},
    {"hostname": "10.170.156.46", "username": username, "password": password},
    {"hostname": "10.78.15.32", "username": username, "password": password},
    {"hostname": "10.78.15.81", "username": username, "password": password},
    {"hostname": "10.78.15.83", "username": username, "password": password}
]

api_endpoint = "http://grs-prd-metadata-manager.eng.vmware.com/v1/health/check/"
webhook_url = "https://hooks.slack.com/services/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"
channel = "#goldrush-alerts"
#command = ["sudo", "systemctl", "status", "httpd"]

#If the status not retunr 200, wait for 1 min, if still same then return error code
#If status retun 200, straight exit and return code
def check_api_status(api_endpoint, timeout=60):
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            response = requests.get(api_endpoint, verify=False)

            logging.info(f'{datetime.now()} - HTTP Status Code: {response.status_code}')
            logging.info(f'{datetime.now()} - Response Content: {response.text}')

            if response.status_code == 200:
                return response.status_code
                break
        except requests.exceptions.RequestException as e:
            raise RuntimeError(f"API request failed: {e}")
        time.sleep(5)
    return response.status_code   
        

def restart_httpd(hostname, username, password):
    try:
     #os.system('source /data/grsadmin/venv/bin/activate && cd /data/grsadmin/core-services/ && fab restart')
     #result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
     #print(result.stdout.decode('utf-8'))
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname, username=username, password=password)
        stdin, stdout, stderr = ssh.exec_command('systemctl restart httpd')
        print(stdout.read().decode('utf-8'))
        print(stderr.read().decode('utf-8'))
    except Exception as e:
        print(f"httpd restart Error on hostname:{hostname}, Error: {e}")
        sys.exit(1)
    finally:
        ssh.close()

def send_slack_message(webhook_url, channel, message):
    payload = {
        "channel": channel,
        "text": message
    }

    headers = {
        "Content-Type": "application/json"
    }

    response = requests.post(webhook_url, data=json.dumps(payload), headers=headers)

def main():

    #This is for issue which unable to resolve domian name, request on api url will fail
    try:
        status_code = check_api_status(api_endpoint)
    except RuntimeError as e:
        for server in servers:
            restart_httpd(server["hostname"], server["username"], server["password"])
            message = f"Failed to request API: {e}. Restarted All API Servers httpd."
            send_slack_message(webhook_url, channel, message)

    if status_code != 200:
        for server in servers:
             restart_httpd(server["hostname"], server["username"], server["password"])
             message = f"GRS API status check failed (Status Code: {status_code}). Restarted All API Servers httpd."
             send_slack_message(webhook_url, channel, message)



if __name__ == "__main__":

    schedule.every(1).minutes.do(main)

while True:
    schedule.run_pending()
    time.sleep(1)