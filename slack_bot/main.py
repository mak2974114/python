import logging
import os
import re

# import slack_bolt
from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler

# Configure logging settings to log to stdout
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# Create a new Slack app and enable Socket Mode
app = App(token=os.environ["SLACK_BOT_TOKEN"])

# Define bot's response in pre-defined templates
templates = {
  'uid': 'To understand what SRP UIDs are and how to create them, please refer to the <https://confluence.eng.vmware.com/display/SRPIPELINE/How+to+create+a+SRP+UID?fromslack|How to create a SRP UID> guide.',
  'submission': 'For information on SRP submissions, the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Submission+FAQ?fromslack|SRP Submission FAQ page> is a valuable resource. It addresses common queries regarding submissions.',
  'provenance': 'For guidance on getting started with provenance submission, see the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Provenance+Contribution+Guide?fromslack|SRP Provenance Contribution Guide>. For other provenance-related queries, the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Submission+FAQ?fromslack|SRP Submission FAQ page> might have the information you are seeking. It contains a section dedicated to provenance concerns.',
  'observer': 'To learn more about Observer integration, you can visit the <https://confluence.eng.vmware.com/display/SRPIPELINE/Observer+Integration+FAQ?fromslack|Observer Integration FAQ page>. It provides insights and answers to common questions regarding Observer integration.',
  '2.5': 'Provenance 2.5 has been deprecated. New integration (mostly from scratch) with provenance 3.0 will be needed going forward. More details in <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Provenance+Contribution+Guide?fromslack|here>.',
  'unauthorized': 'To learn more about what access is needed, please refer to <https://confluence.eng.vmware.com/display/SRPIPELINE/Onboarding+to+SRP+APIs#OnboardingtoSRPAPIs-UsingtheSRPAPIwithyourVMwareAccount?fromslack|the document here>.  It is likely that you are not in the mts1@vmware.com LDAP group.',
  'template': 'For information about release templates, please see <https://confluence.eng.vmware.com/display/SRPIPELINE/Creating+and+editing+SRP+template+releases?fromslack|Creating and Editig SRP template releases>.',
  'incorporated': 'For information on how to fix incorporated issues, please see <https://confluence.eng.vmware.com/display/SRPIPELINE/How+to+fix+incorporated+compliance+failures?fromslack|here>',
  'python': 'For guidance on how to contribute provenance for Python applications, please search for python on the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index?fromslack|SRP Example Index> page.',
  'npm': 'For guidance on how to contribute provenance for node.js/npm applications, please search for npm on the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index?fromslack|SRP Example Index> page.',
  'node.js': 'For guidance on how to contribute provenance for node.js/npm applications, please search for npm on the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index?fromslack|SRP Example Index> page.',
  'go': 'For guidance on how to contribute provenance for Golang applications, please search for golang on the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index?fromslack|SRP Example Index> page.',
  'golang': 'For guidance on how to contribute provenance for Golang applications, please search for golang on the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index?fromslack|SRP Example Index> page.',
  'gradle': 'For guidance on how to contribute provenance for Java/Gradle applications, please search for gradle on the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index?fromslack|SRP Example Index> page.',
  'maven': 'For guidance on how to contribute provenance for Java/Maven applications, please search for maven on the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index?fromslack|SRP Example Index> page.',
  'gitlab': 'For guidance on how to contribute provenance from Gitlab CI, please see the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index#SRPExampleIndex-GitlabCIexamples?fromslack|Gitlab CI Examples>.',
  'jenkins': 'For guidance on how to contribute provenance from Jenkins, please see <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index#SRPExampleIndex-Jenkins?fromslack|Jenkins>.', 
  'gobuild': 'For guidance on how to contribute provenance from Gobuild, please see <https://confluence.eng.vmware.com/display/SRPIPELINE/Provenance+3.0+Contribution+for+Gobuild?fromslack|Provenance 3.0 Contribution for Gobuild>, and also the <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Example+Index#SRPExampleIndex-GobuildSRPExamples?fromslack|Gobuild SRP Examples>.', 
  'docker': 'For guidance on how to contribute provenance for Docker builds, please see <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Docker+Build+Strategies?fromslack|SRP Docker Build Strategies>',
  'oci': 'For guidance on how to contribute provenance for Docker builds and OCI images, please see <https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Docker+Build+Strategies?fromslack|SRP Docker Build Strategies>',
}

# Listens to app mentions
@app.event("app_mention")
def handle_app_mentions(body, say):
    user = body["event"]["user"]
    channel = body["event"]["channel"]
    message = body["event"]["text"]
    thread_ts = body["event"]["ts"]  # Get the 'ts' value of the original message

    logging.info(f'User mention received from user {user} in channel {channel}: {message}')

    # Remove the bot mention from the message
    cleaned_message = re.sub(r'<@[^>]+>', '', message)

    # Split the cleaned message into words using regex to handle punctuation
    words = re.split(r"[^\w']+", cleaned_message)

    # Initialize template as None
    template = None

    # Iterate through each word in the cleaned message
    for word in words:
        # Check if the word is a key in the templates dictionary
        # Convert the key to lowercase for case-insensitive matching
        template = templates.get(word.lower())
        # If a template is found, break out of the loop
        if template:
            break

    # If the word is a key in the templates dictionary
    if template:
        response_text = f"Hello <@{user}>, {template}"
        say(response_text, channel=channel, thread_ts=thread_ts)
        logging.info(f'Bot replied to user {user} in channel {channel}: {response_text}')
    # Else use a general response
    else:
        response_text = f"Hello <@{user}>, I\'m unable to provide a precise answer to your question. However, you may find useful information in the SRP User Documentation https://confluence.eng.vmware.com/display/SRPIPELINE/SRP+Compliance+User+Guide?fromslack. Meanwhile, the SRP team might be able to provide further assistance. Thank you for your patience and understanding."
        say(response_text, channel=channel, thread_ts=thread_ts)
        logging.info(f'Bot replied to user {user} in channel {channel}: {response_text}')

# Error handler
@app.error
def error_handler(error, body, logger):
    logger.exception(error)
    if "channel" in body:
        channel_id = body["channel"]
        app.client.chat_postMessage(channel=channel_id, text=f"An error occurred: {error}")

# Start the Bolt app
if __name__ == '__main__':
  try:
    handler = SocketModeHandler(app, os.environ["SLACK_APP_TOKEN"])
    handler.start()
  except Exception as e:
    logging.error(e)